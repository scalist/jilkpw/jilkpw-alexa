# jilkpw-alexa

## About

`jilkpw-alexa` is a small Amazon Echo app/skill for getting a random listing and returning it to the user. It makes fairly heavy use of [Jilk.pw's Python Wrapper](https://gitlab.com/scalist/jilkpw/jilkpw-python-wrapper) to retrive listings.

This project runs in a lonely single Docker container with no further Kubernetes/similar DevOps planned (due to this being a low traffic skill).

*Please do not mind the exessive commit count for this small app, was using this as a side project to test the `cryptography` package on Alpine. Note to self, use Debian for this..*

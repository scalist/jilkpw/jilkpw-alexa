FROM python:3.6
ADD . /code
WORKDIR /code
RUN pip install --force-reinstall pip==9.0.3
RUN pip install flask jilkpw-py flask-ask requests 'cryptography<2.2'
CMD ["python", "app.py"]

import random
from jilkpw_py import JilkpwWrapper
from flask import Flask, render_template
from flask_ask import Ask, statement, question

app = Flask(__name__)
ask = Ask(app, "/")
jilkpw_wrapper = JilkpwWrapper()


jilk_view_question = "would you like to hear a random Jilk.pw server"


def _get_random_listing():
    listings = jilkpw_wrapper.all()
    random_listing = listings[random.choice(list(listings.keys()))]

    return "{0} on Jilk.pw. {0} has {1} vote(s) and {2} members".format(
        random_listing["name"], random_listing["votes"], random_listing["members"]
    )


@app.route("/")
def index():
    return (
        "Subdomain for the Jilk.pw Alexa skill found on it's respective skill store.<br><br>"
        'Please click <a href="https://jilk.pw">here</a> to return to Jilk.pw'
    )


@ask.launch
def launch_question():
    text = f"Welcome to Jilk.pw's Amazon Echo skill. {jilk_view_question}?"
    return question(text).reprompt(f"Sorry, I don't understand, {jilk_view_question}?")


@ask.intent("AMAZON.YesIntent")
def random_jilk():
    return question(
        f"{_get_random_listing()}. Would you like to hear another server?"
    ).reprompt(f"Sorry, I don't understand, {jilk_view_question} again?")


@ask.intent("AMAZON.HelpIntent")
def help_user():
    return question(
        f"This skill can simply be used by saying the word yes. So then, {jilk_view_question}?"
    )

@ask.intent("AMAZON.NoIntent")
@ask.intent("AMAZON.ExitIntent")
@ask.intent("AMAZON.StopIntent")
@ask.intent("AMAZON.CancelIntent")
def exit_skill():
    return statement("Sorry to see you go.")


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8084)
